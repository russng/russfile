# 
# Makefile
#

none:

install:
	@if [ "${INSTALL_DIR}" = "" ]; then \
		echo "error: INSTALL_DIR is not defined"; \
		exit 1; \
	fi
	mkdir -p "${INSTALL_DIR}"
	(cd static; tar cf - .) | (cd $(INSTALL_DIR); tar xvf -)
