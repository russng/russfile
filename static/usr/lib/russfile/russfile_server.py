#! /usr/bin/env python3
#
# russfile_server.py

# license--start
#
# Copyright 2015 John Marshall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# license--end

import glob
import grp
import os
import os.path
import pwd
import shutil
import stat
import sys
import tarfile
import traceback

sys.path.insert(1, "/usr/lib/russng")
import pyruss
from pyruss import strtobytes, bytestostr

BSIZE = None
BSIZE_DEFAULT = 1<<20
BSIZE_MAX = None
BSIZE_MAX_DEFAULT = 8<<20
MSG_BADCHDIR = "error: cannot change directory"
MSG_BADPATH = "error: bad path/paths"
MSG_FAIL = "error: failed"
MSG_FILEEXISTS = "error: file exists"
UMASK = None
UMASK_DEFAULT = 0o022

HELP = """\
Perform basic file operations.

All paths are expanded using UNIX style pathname pattern expansion
(* and ? wildcards, character ranges within []). A non-match results
in the original path being used.

Common options:
-B <size>
    Block size to use for transfer operations. Default is %s
    with a maximum of %s.
-C <dir>
    Change working directory on service side. Default is the home of
    the requesting user.
-f  Force operation. Depending on context, may mean overwrite an
    existing file object or ignore error and proceed.
-U <umask>
    Perform operations using a specific umask (default umask is 0%o);
    specifying a umask is almost always a good idea to avoid
    surprises from the default. <umask> may be specified in octal
    by a leading 0.

/chmod [-C <dir>] <mode> <path>
    Change file mode.

/chown [-C <dir>] <owner>[:<group>] <path>
    Change file ownership.

/copy [-C <dir>] [-f] [-U <umask>] <srcpath> <dstpath>
    Copy file srcpath to dstpath. If dstpath is a directory, srcpath
    will be copied into it.

/get [-B <size>] [-C <dir>] <path> [...]
    Read one or more files to stdout.

/gets [-B <size>] [-C <dir>] [-o <offset>] [-s <size>] [-w end|set] <path>
    Read file segment to stdout. Use -o and -w options to position
    the file pointer before reading. Use -s to limit size of data
    read from offset. Default offset is 0, default whence is end.
    Default size is unlimited.

/listdir <path>
    Return a list of directory entries, one per line.

/mkdir [-C <dir>] [-m <mode>] [-p] [-U <umask>] <path>
    Create directory. Default file mode is 0700. Use -p to create
    parent directories as needed.

/move [-C <dir>] [-f] <srcpath> <dstpath>
    Move file from srcpath to dstpath. If dstpath is an existing
    directory, srcpath will be moved into it.

/pack [-B <size>] [-C <dir>] [-U <umask>] <packtype> <path> [...]
    Pack files and write to stdout. Use -C to change directory
    before packing. <packtype> must be one of --tar, --tbz2, --tgz.

/put [-B <size>] [-C <dir>] [-f] [-m <mode>] [-U <umask>] <path>
    Write stdin to file.

/puts [-B <size>] [-C <dir>] [-m <mode>] [-o <offset>] [-U <umask>] [-w end|set] <path>
    Write stdin to file segment. Default file mode is 0600. Use -o
    and -w options to position the file pointer before writing.
    Default offset is 0, default whence is end, which is equivalent
    to append.

/remove [-C <dir>] [-f] <path>
    Remove file.

/rmdir [-C <dir>] [-p] <path>
    Remove empty directory. Use -p to remove parent directories.

/stat [-C <dir>] [-L] <path>
    Get path file status. Use -L to dereference <path>.

/symlink [-C <dir>] <srcpath> <linkname>
    Create symbolic link where <linkname> points to <srcpath>.

/touch [-C <dir>] [-U <umask>] <path>
    Change file timestamps.

/truncate [-C <dir>] [-s <size>] [-U <umask>] <path>
    Shrink or extend file size. Default is size of 0.

/unpack [-B <size>] [-C <dir>] [-U <umask>] <packtype>
    Unpack files read from stdin. <packtype> must be one of --tar,
    --tbz2, --tgz.
""" % (BSIZE_DEFAULT, BSIZE_MAX_DEFAULT, UMASK_DEFAULT)

def chdir(path):
    try:
        os.chdir(path)
    except:
        return -1
    return 0

def chdir_expand_path_with_exit(conn, chpath, path):
    """Helper to call chdir and expand_path with possibility to
    report message to client and exit. Expanded path is returned.
    """
    if chpath == None:
        chpath = os.path.expanduser("~")
    if chdir(expand_path(chpath)) < 0:
        conn.fatal(MSG_BADCHDIR, pyruss.RUSS_EXIT_FAILURE)
        sys.exit(0)

    path = expand_path(path)
    if path == None:
        conn.fatal(MSG_BADPATH, pyruss.RUSS_EXIT_FAILURE)
        sys.exit(0)

    return path

def expand_path(path, onepath=True):
    path = os.path.expanduser(path)
    paths = glob.glob(path)
    if len(paths) == 0:
        paths = [path]
    if onepath:
        if len(paths) > 1:
            return None
        else:
            return paths[0]
    else:
        return paths

def filemode2int(mode):
    if mode.isdigit():
        if mode.startswith("0"):
            mode = int(mode, 8)
        else:
            mode = int(mode)
    else:
        # TODO
        mode = None
    return mode

def group2gid(name):
    try:
        if name.isdigit():
            gid = int(name)
        else:
            gr = grp.getgrnam(name)
            gid = gr.gr_gid
        return gid
    except:
        return None

def user2uid(name):
    try:
        if name.isdigit():
            uid = int(name)
        else:
            pw = pwd.getpwnam(name)
            uid = pw.pw_uid
        return uid
    except:
        return None

class ServiceTree:

    def __init__(self):
        self.root = pyruss.ServiceNode.new("", self.svc_root)
        self.root.add("chmod", self.svc_chmod)
        self.root.add("chown", self.svc_chown)
        self.root.add("copy", self.svc_copy)
        self.root.add("get", self.svc_get)
        self.root.add("listdir", self.svc_listdir)
        self.root.add("mkdir", self.svc_mkdir)
        self.root.add("move", self.svc_move)
        self.root.add("pack", self.svc_pack)
        self.root.add("put", self.svc_put)
        self.root.add("puts", self.svc_puts)
        self.root.add("remove", self.svc_remove)
        self.root.add("rmdir", self.svc_rmdir)
        self.root.add("stat", self.svc_stat)
        self.root.add("symlink", self.svc_symlink)
        #self.root.add("test", self.svc_test)
        self.root.add("touch", self.svc_touch)
        self.root.add("truncate", self.svc_truncate)
        self.root.add("unpack", self.svc_unpack)

    def svc_root(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_chmod(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                mode = None
                path = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif len(args) == 1:
                        mode = filemode2int(arg)
                        path = args.pop(0)
                    else:
                        raise Exception()

                if None in [mode, path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                path = chdir_expand_path_with_exit(conn, chpath, path)
                os.chmod(path, mode)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_chown(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                owner = None
                path = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif len(args) == 1:
                        owner = arg
                        path = args.pop(0)
                    else:
                        raise Exception()

                if None in [owner, path]:
                    raise Exception

                if ":" in owner:
                    owner, group = owner.split(":", 1)
                    uid = user2uid(owner)
                    gid = group2gid(group)
                else:
                    uid = user2uid(owner)
                    gid = -1
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                path = chdir_expand_path_with_exit(conn, chpath, path)
                os.chown(path, uid, gid)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_copy(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                force = False
                dstpath = None
                srcpath = None
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-f":
                        force = True
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif len(args) == 1:
                        srcpath = arg
                        dstpath = args.pop(0)
                    else:
                        raise Exception()

                if None in [srcpath, dstpath]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                os.umask(umask)
                srcpath = chdir_expand_path_with_exit(conn, chpath, srcpath)
                dstpath = chdir_expand_path_with_exit(conn, chpath, dstpath)

                if os.path.isfile(dstpath) and not force:
                    conn.fatal(MSG_FILEEXISTS, pyruss.RUSS_EXIT_FAILURE)
                    sys.exit(0)

                shutil.copy(srcpath, dstpath)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_get(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                bsize = BSIZE
                chpath = None
                paths = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-B" and args:
                        bsize = min(int(args.pop(0)), BSIZE_MAX)
                    elif arg == "-C" and args:
                        chpath = args.pop(0)
                    else:
                        paths = [arg]+args[:]
                        del args[:]

                if None in [paths]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                f = None
                outfd = conn.get_fd(1)

                for path in paths:
                    path = chdir_expand_path_with_exit(conn, chpath, path)

                    nbytes = os.path.getsize(path)
                    f = open(path, "rb")
                    while nbytes:
                        buf = f.read(bsize)
                        if buf == b"":
                            break
                        os.write(outfd, buf)
                        nbytes -= len(buf)
                    f.close()
                    f = None
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                if f:
                    f.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_gets(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                bsize = BSIZE
                chpath = None
                offset = 0
                path = None
                size = None
                whence = os.SEEK_END

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-B" and args:
                        bsize = min(int(args.pop(0)), BSIZE_MAX)
                    elif arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-o" and args:
                        offset = int(args.pop(0))
                    elif arg == "-s" and args:
                        size = int(args.pop(0))
                    elif arg == "-w" and args:
                        arg = args.pop(0)
                        if arg == "end":
                            whence = os.SEEK_END
                        elif arg == "set":
                            whence = os.SEEK_SET
                        else:
                            raise Exception()
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                f = None

                path = chdir_expand_path_with_exit(conn, chpath, path)

                outfd = conn.get_fd(1)
                f = open(path, "rb")
                f.seek(offset, whence)

                while size == None or nbytes < size:
                    buf = f.read(bsize)
                    if buf == b"":
                        # TODO: support for streaming and log files?
                        break
                    os.write(outfd, buf)
                    nbytes -= len(buf)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                f.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_listdir(self, sess):
        # TODO: prevent memory exhaustion?
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                args = req.get_args()
                path = args.pop(0)

                if args:
                    raise Exception()
                path = expand_path(path)
                if path == None:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                outfd = conn.get_fd(1)
                names = os.listdir(path)
                if names:
                    l = []
                    for name in names:
                        l.append(name)
                        if len(l) >= 50:
                            os.write(outfd, strtobytes("\n".join(l)))
                            l = []
                    else:
                        if l:
                            os.write(outfd, strtobytes("\n".join(l)))
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_mkdir(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                mode = filemode2int("0700")
                parents = False
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-m" and args:
                        mode = filemode2int(args.pop(0))
                    elif arg == "-p":
                        parents = True
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                os.umask(umask)
                path = chdir_expand_path_with_exit(conn, chpath, path)
                if not os.path.isdir(path):
                    if parents:
                        os.makedirs(path, mode)
                    else:
                        os.mkdir(path, mode)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_move(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                force = False
                dstpath = None
                srcpath = None
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-f":
                        force = True
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif len(args) == 1:
                        srcpath = arg
                        dstpath = args.pop(0)
                    else:
                        raise Exception()

                if None in [srcpath, dstpath]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                os.umask(umask)
                srcpath = chdir_expand_path_with_exit(conn, chpath, srcpath)
                dstpath = chdir_expand_path_with_exit(conn, chpath, dstpath)

                if os.path.isfile(dstpath) and not force:
                    conn.fatal(MSG_FILEEXISTS, pyruss.RUSS_EXIT_FAILURE)
                    sys.exit(0)

                shutil.move(srcpath, dstpath)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_pack(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                bsize = BSIZE
                chpath = None
                packtype = None
                paths = None
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-B" and args:
                        bsize = min(int(args.pop(0)), BSIZE_MAX)
                    elif arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg in ["--tar", "--tbz2", "--tgz", "-tar", "-tgz"] and args:
                        if arg.startswith("--"):
                            packtype = arg[2:]
                        else:
                            # temporary
                            packtype = arg[1:]
                        paths = args[:]
                        del args[:]
                        break
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    else:
                        raise Exception()

                if None in [packtype, paths]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                outf = None
                tf = None

                os.umask(umask)

                if chpath and chdir(expand_path(chpath)) < 0:
                    conn.fatal(MSG_BADCHDIR, pyruss.RUSS_EXIT_FAILURE)
                    sys.exit(0)

                _paths = []
                for path in paths:
                    _paths.extend(expand_path(path, False))
                paths = _paths
                if not len(paths):
                    conn.fatal(MSG_BADPATH, pyruss.RUSS_EXIT_FAILURE)
                    sys.exit(0)

                if packtype == "tar":
                    tarmode = "w|"
                elif packtype == "tbz2":
                    tarmode = "w|bz2"
                elif packtype == "tgz":
                    tarmode = "w|gz"

                outf = os.fdopen(conn.get_fd(1), "wb", bsize)
                tf = tarfile.open(mode=tarmode, fileobj=outf, bufsize=bsize)
                for path in paths:
                    tf.add(path)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                if tf:
                    tf.close()
                if outf:
                    outf.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_put(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                bsize = BSIZE
                chpath = None
                force = False
                mode = None
                path = None
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-B" and args:
                        bsize = min(int(args.pop(0)), BSIZE_MAX)
                    elif arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-f":
                        force = True
                    elif arg == "-m" and args:
                        mode = filemode2int(args.pop(0))
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                f = None

                os.umask(umask)
                path = chdir_expand_path_with_exit(conn, chpath, path)

                if os.path.exists(path) and not force:
                    conn.fatal(MSG_FILEEXISTS, pyruss.RUSS_EXIT_FAILURE)
                    sys.exit(0)

                f = open(path, "wb")
                if mode != None:
                    os.chmod(path, mode)

                infd = conn.get_fd(0)
                while True:
                    buf = os.read(infd, bsize)
                    if buf == b"":
                        break
                    f.write(buf)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                if f:
                    f.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_puts(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                bsize = BSIZE
                chpath = None
                mode = None
                offset = 0
                whence = os.SEEK_END
                path = None
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-B" and args:
                        bsize = min(int(args.pop(0)), BSIZE_MAX)
                    elif arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-m" and args:
                        mode = filemode2int(args.pop(0))
                    elif arg == "-o" and args:
                        offset = int(args.pop(0))
                    elif arg == "-w" and args:
                        arg = args.pop(0)
                        if arg == "end":
                            whence = os.SEEK_END
                        elif arg == "set":
                            whence = os.SEEK_SET
                        else:
                            raise Exception()
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                f = None

                os.umask(umask)
                path = chdir_expand_path_with_exit(conn, chpath, path)

                try:
                    f = open(path, "rb+")
                except:
                    # create new file
                    f = open(path, "wb")
                if mode != None:
                    os.chmod(path, mode)
                f.seek(offset, whence)

                infd = conn.get_fd(0)
                while True:
                    buf = os.read(infd, bsize)
                    if buf == b"":
                        break
                    f.write(buf)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                if f:
                    f.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_remove(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                force = False
                path = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-f":
                        force = True
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                path = chdir_expand_path_with_exit(conn, chpath, path)
                try:
                    os.remove(path)
                except:
                    if force:
                        pass
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_rmdir(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                parents = False
                path = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-p":
                        parents = True
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                path = chdir_expand_path_with_exit(conn, chpath, path)
                if parents:
                    os.removedirs(path)
                else:
                    os.rmdir(path)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_stat(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                deref = False
                path = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-L":
                        deref = True
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                path = chdir_expand_path_with_exit(conn, chpath, path)
                if deref:
                    path = os.path.realpath(path)
                st = os.stat(path)

                fmt = stat.S_IFMT(st.st_mode)
                if stat.S_ISCHR(fmt):
                    ftype = "chr"
                elif stat.S_ISBLK(fmt):
                    ftype = "blk"
                elif stat.S_ISDIR(fmt):
                    ftype = "dir"
                elif stat.S_ISREG(fmt):
                    ftype = "reg"
                elif stat.S_ISFIFO(fmt):
                    ftype = "fifo"
                elif stat.S_ISLNK(fmt):
                    ftype = "symlink"
                elif stat.S_ISSOCK(fmt):
                    ftype = "sock"
                else:
                    ftype = "unk"

                l = [
                    "mode=%s" % (st.st_mode,),
                    "inode=%s" % (st.st_ino,),
                    "device=%s" % (st.st_dev,),
                    "nlink=%s" % (st.st_nlink,),
                    "uid=%s" % (st.st_uid,),
                    "gid=%s" % (st.st_gid,),
                    "size=%s" % (st.st_size,),
                    "atime=%s" % (st.st_atime,),
                    "mtime=%s" % (st.st_mtime,),
                    "ctime=%s" % (st.st_ctime,),
                    "ftype=%s" % (ftype,),
                ]
                outfd = conn.get_fd(1)
                os.write(outfd, strtobytes("%s\n" % "\n".join(l)))
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_symlink(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                linkname = None
                srcpath = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif len(args) == 1:
                        srcpath = arg
                        linkname = args.pop(0)
                    else:
                        raise Exception()

                if None in [srcpath, linkname]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                linkname = chdir_expand_path_with_exit(conn, chpath, linkname)
                os.symlink(srcpath, linkname)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_test(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                path = None
                tst = None

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif len(args) == 1:
                        tst = arg
                        path = args.pop(0)
                    else:
                        raise Exception()

                if None in [path, tst]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                path = chdir_expand_path_with_exit(conn, chpath, path)

                conn.fatal("error: not yet implemented", pyruss.RUSS_EXIT_FAILURE)
            except:
                conn.exit(pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_touch(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                path = None
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                os.umask(umask)
                path = chdir_expand_path_with_exit(conn, chpath, path)
                if os.path.exists(path):
                    open(path, "rb")
                else:
                    open(path, "wb")
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_truncate(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                chpath = None
                path = None
                size = 0
                umask = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg == "-s" and args:
                        size = int(args.pop(0))
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    elif not args:
                        path = arg
                        break
                    else:
                        raise Exception()

                if None in [path]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                f = None

                os.umask(umask)
                path = chdir_expand_path_with_exit(conn, chpath, path)

                f = open(path, "wb")
                f.truncate(size)
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                if f:
                    f.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

    def svc_unpack(self, sess):
        conn = sess.get_sconn()
        req = sess.get_request()
        if req.opnum == pyruss.RUSS_OPNUM_EXECUTE:
            try:
                bsize = BSIZE
                chpath = None
                packtype = None
                umask  = UMASK

                args = req.get_args()
                while args:
                    arg = args.pop(0)
                    if arg == "-B" and args:
                        bsize = min(int(args.pop(0)), BSIZE_MAX)
                    elif arg == "-C" and args:
                        chpath = args.pop(0)
                    elif arg in ["--tar", "--tbz2", "--tgz", "-tar", "-tgz"]:
                        if arg.startswith("--"):
                            packtype = arg[2:]
                        else:
                            # temporary
                            packtype = arg[1:]
                    elif arg == "-U" and args:
                        umask = filemode2int(args.pop(0))
                    else:
                        raise Exception()

                if None in [packtype]:
                    raise Exception()
            except:
                conn.fatal(pyruss.RUSS_MSG_BADARGS, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)

            try:
                inf = None
                tf = None

                os.umask(umask)
                if chpath and chdir(expand_path(chpath)) < 0:
                    conn.fatal(MSG_BADCHDIR, pyruss.RUSS_EXIT_FAILURE)
                    sys.exit(0)

                if packtype == "tar":
                    tarmode = "r|"
                elif packtype == "tbz2":
                    tarmode = "r|bz2"
                elif packtype == "tgz":
                    tarmode = "r|gz"

                inf = os.fdopen(conn.get_fd(0), "rb", bsize)
                tf = tarfile.open(mode=tarmode, fileobj=inf, bufsize=bsize)
                tf.extractall()
            except:
                traceback.print_exc()
                conn.fatal(MSG_FAIL, pyruss.RUSS_EXIT_FAILURE)
                sys.exit(0)
            finally:
                if tf:
                    tf.close()
                if inf:
                    inf.close()
            conn.exit(pyruss.RUSS_EXIT_SUCCESS)

if __name__ == "__main__":
    try:
        if len(sys.argv) < 2:
            raise Exception()
        config = pyruss.Conf()
        config.init(sys.argv)
    except:
        traceback.print_exc()
        sys.stderr.write("error: bad/missing arguments\n")
        sys.exit(1)

    try:
        # delete HOME so that expanduser will work
        if "HOME" in os.environ:
            del os.environ["HOME"]

        BSIZE_MAX = config.getint("other", "bsize_max", BSIZE_MAX_DEFAULT)
        BSIZE = min(config.getint("other", "bsize", BSIZE_DEFAULT), BSIZE_MAX)
        UMASK = config.get("other", "umask")
        if UMASK == None:
            UMASK = UMASK_DEFAULT

        svr = pyruss.server.init(config)
        if svr == None \
            or svr.set_root(ServiceTree().root) < 0 \
            or svr.set_type(pyruss.RUSS_SVR_TYPE_FORK) < 0 \
            or svr.set_autoswitchuser(1) < 0 \
            or svr.set_matchclientuser(1) < 0 \
            or svr.set_allowrootuser(1) < 0 \
            or svr.set_help(HELP) < 0:
            raise Exception()
        svr.loop()
    except:
        traceback.print_exc()
        sys.stderr.write("error: cannot set up server\n")
        sys.exit(1)
